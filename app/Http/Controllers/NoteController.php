<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\View\View;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
//        $notes = Note::query()
//            ->where(['user_id' => Auth::id()])
//            ->latest('updated_at')
//            ->paginate(5);

//        $notes = Auth::user()
//            ->notes()
//            ->latest('updated_at')
//            ->paginate(5);

        $notes = Note::query()
            ->whereBelongsTo(Auth::user())
            ->latest('updated_at')
            ->paginate(5);

        return view('notes.index')->with('notes', $notes);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('notes.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'max:120'],
            'text' => ['required']
        ]);

//        Note::query()
//            ->create([
//                'uuid' => Str::uuid(),
//                'user_id' => Auth::id(),
//                'title' => $request->get('title'),
//                'text' => $request->get('text'),
//            ]);

        Auth::user()
            ->notes()
            ->create([
                'uuid' => Str::uuid(),
                'title' => $request->get('title'),
                'text' => $request->get('text'),
            ]);

        return to_route('notes.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Note $note): View
    {
//        return $note['user_id'] !== Auth::id() ? abort(403) : view('notes.show')->with('note', $note);
        return $note->user->is(Auth::user()) ? view('notes.show')->with('note', $note) : abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Note $note): View
    {
        return $note['user_id'] !== Auth::id() ? abort(403) : view('notes.edit')->with('note', $note);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Note $note): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'max:120'],
            'text' => ['required']
        ]);

        if (!$note->user->is(Auth::user())) abort(403);

        $note->update([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
        ]);

        return to_route('notes.show', $note)->with('success', 'Note updates successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Note $note): RedirectResponse
    {
        if (!$note->user->is(Auth::user())) abort(403);

        $note->delete();

        return to_route('notes.index')->with('success', 'Note moved to trash');
    }
}
