<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TrashedNoteController extends Controller
{
    public function index():View
    {
        $notes = Note::query()
            ->whereBelongsTo(Auth::user())
            ->onlyTrashed()
            ->latest('updated_at')
            ->paginate(5);

        return view('notes.index')->with('notes', $notes);
    }

    public function show(Note $note): View
    {
        return $note->user->is(Auth::user()) ? view('notes.show')->with('note', $note) : abort(403);
    }

    public function update(Note $note): RedirectResponse
    {
        if (!$note->user->is(Auth::user())) abort(403);

        $note->restore();

        return to_route('notes.show', $note)->with('success', 'Note restored');
    }

    public function destroy(Note $note)
    {
        $note->user->is(Auth::user()) ? $note->forceDelete() : abort(403);

        return to_route('trashed.index')->with('success', 'Note deleted forever');
    }
}
